module.exports = {
  NYT_API: {
    ROOT: "https://api.nytimes.com/svc/movies/v2/reviews/search.json?",
    API_KEY_QUERY: "&api-key=",
    KEY: "NYT_API_KEY_HERE",
  },
  OMDB_API: {
    ROOT: "http://www.omdbapi.com/?apikey=",
    KEY: "OMDB_API_KEY_HERE",
  },
  ROUTES: {
    OMDB_SEARCH: "/search",
    OMDB_MOVIE: "/movie",
    NYT_REVIEW: "/nyt-review",
  },
};
