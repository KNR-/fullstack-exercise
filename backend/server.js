const port = 3001;
const express = require("express");
const axios = require("axios");
const cors = require("cors");
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  })
);
app.use(cors());

const CONST = require("./config/config");

app.get("/", (req, res) => {
  res.send("Server Running!");
});

app.post(CONST.ROUTES.OMDB_SEARCH, async (req, res) => {
  const params = req.body;
  if (!params.title) {
    return res.status(500).send({ Error: "Title missing from search params!" });
  }

  const search = "&s=";
  const title = params.title;
  const type = params.type ? "&type=" + params.type : "";
  const year = params.year ? "&y=" + params.year : "";
  const page = params.page ? "&page=" + params.page : "";

  const queryParams = "".concat(search, title, type, year, page);
  const query = CONST.OMDB_API.ROOT + CONST.OMDB_API.KEY + queryParams;
  console.log("Query:", query);
  try {
    const response = await axios.get(query);
    return res.status(200).send(response.data);
  } catch (e) {
    return res.status(500).send({ Error: "Error while handling data: " + e });
  }
});

app.post(CONST.ROUTES.OMDB_MOVIE, async (req, res) => {
  const params = req.body;
  if (!params.imdbId) {
    return res.status(500).send({ Error: "imdbId missing from params!" });
  }

  const search = "&i=";
  const queryParams = "".concat(search, params.imdbId);
  const query = CONST.OMDB_API.ROOT + CONST.OMDB_API.KEY + queryParams;
  console.log("Query:", query);
  try {
    const response = await axios.get(query);
    return res.status(200).send(response.data);
  } catch (e) {
    return res.status(500).send({ Error: "Error while handling data: " + e });
  }
});

app.post(CONST.ROUTES.NYT_REVIEW, async (req, res) => {
  const params = req.body;
  const queryParams = "query=" + params.title;

  const query =
    CONST.NYT_API.ROOT +
    queryParams +
    CONST.NYT_API.API_KEY_QUERY +
    CONST.NYT_API.KEY;

  console.log("Query:", query);
  try {
    const response = await axios.get(query);
    return res.status(200).send(response.data);
  } catch (e) {
    return res.status(500).send({ Error: "Error while handling data: " + e });
  }
});

app.listen(port, () => {
  console.log(`Backend listening at http://localhost:${port}`);
});
