import { createSlice } from "@reduxjs/toolkit";
import { API_ROUTES } from "../../config/config";
const axios = require("axios");

export const movieSlice = createSlice({
  name: "movie",
  initialState: {
    searchByTitleText: "",
    searchByYearText: "",
    searchByTypeText: "",
    movieList: {},
    page: 0,
    movie: {},
    nytReviewsList: {},
    loading: false,
  },
  reducers: {
    setSearchByTitleText: (state, action) => {
      state.searchByTitleText = action.payload;
    },
    setSearchByYearText: (state, action) => {
      state.searchByYearText = action.payload;
    },
    setSearchByTypeText: (state, action) => {
      state.searchByTypeText = action.payload;
    },
    setMovieListData: (state, action) => {
      state.movieList = action.payload;
    },
    setMovieListPage: (state, action) => {
      state.page = action.payload;
    },
    setMovieData: (state, action) => {
      state.movie = action.payload;
    },
    setNytReviews: (state, action) => {
      state.nytReviewsList = action.payload;
    },
    setLoadingStatus: (state, action) => {
      state.loading = action.payload;
    },
  },
});

export const {
  setSearchByTitleText,
  setSearchByYearText,
  setSearchByTypeText,
  setMovieListData,
  setMovieData,
  setNytReviews,
  setLoadingStatus,
  setMovieListPage,
} = movieSlice.actions;

export const getMovieList = (state) => state.movie.movieList;
export const getMovie = (state) => state.movie.movie;
export const isLoading = (state) => state.movie.loading;
export const getNytReviews = (state) => state.movie.nytReviewsList;
export const getSearchByTitleText = (state) => state.movie.searchByTitleText;
export const getSearchByYearText = (state) => state.movie.searchByYearText;
export const getSearchByTypeText = (state) => state.movie.searchByTypeText;
export const getMovieListPages = (state) => state.movie.page;

export const getMovieListByInput = (params) => async (dispatch) => {
  try {
    dispatch(setLoadingStatus(true));
    const response = await axios.post(API_ROUTES.SEARCH, {
      title: params.title,
      year: params.year,
      type: params.type,
      page: params.page,
    });

    dispatch(setLoadingStatus(false));
    dispatch(setMovieListData(response.data));
    const totalPages = Math.round(response.data.totalResults / 10);
    dispatch(setMovieListPage(totalPages));
  } catch (e) {}
};

export const getMovieDataByimdbId = (imdbId) => async (dispatch) => {
  try {
    dispatch(setLoadingStatus(true));
    const response = await axios.post(API_ROUTES.MOVIE, {
      imdbId: imdbId,
    });

    dispatch(setMovieData(response.data));

    const nytReviewParams = {
      title: response.data.Title,
      year: response.data.Year,
    };

    await dispatch(getMovieReviewsFromNyt(nytReviewParams));
    dispatch(setLoadingStatus(false));
  } catch (e) {}
};

export const getMovieReviewsFromNyt = (params) => async (dispatch) => {
  try {
    const response = await axios.post(API_ROUTES.NYT_REVIEW, {
      title: params.title,
      openingDate: params.year,
    });

    dispatch(setNytReviews(response.data));
  } catch (e) {}
};

export default movieSlice.reducer;
