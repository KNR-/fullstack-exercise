import { configureStore } from "@reduxjs/toolkit";
import movieReducer from "../app/reducers/movieStore";

export default configureStore({
  reducer: {
    movie: movieReducer,
  },
});
