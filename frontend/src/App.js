import React from "react";
import "./App.css";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MovieSearchView from "./views/MovieSearchView/MovieSearchView";
import MovieView from "./views/MovieView/MovieView";

function App() {
  return (
    <div>
      <Router>
        <div className="container">
          <Switch>
            <Route path="/:id">
              <MovieView />
            </Route>
            <Route path="/">
              <MovieSearchView />
            </Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}

export default App;
