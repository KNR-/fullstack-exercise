import React, { useEffect } from "react";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";
import { useDispatch } from "react-redux";
import {
  getMovie,
  getMovieDataByimdbId,
  isLoading,
  getNytReviews,
  setMovieData,
  setNytReviews,
} from "../../app/reducers/movieStore";
import { useSelector } from "react-redux";
import "./MovieView.css";
import LoadingIndicator from "../../components/LoadingIndicator/LoadingIndicator";
import NytMovieReviewItem from "../../components/NytMovieReviewItem/NytMovieReviewItem";

function MovieView() {
  const { id } = useParams();
  const dispatch = useDispatch();
  const movie = useSelector(getMovie);
  const nytReviews = useSelector(getNytReviews);
  const showLoader = useSelector(isLoading);

  useEffect(() => {
    dispatch(getMovieDataByimdbId(id));
    return () => {
      dispatch(setMovieData({}));
      dispatch(setNytReviews({}));
    };
  }, [dispatch, id]);

  const renderCardData = () => {
    if (!movie) {
      return;
    }
    let elements = [];
    for (const [key, value] of Object.entries(movie)) {
      if (!Array.isArray(value) && key !== "Poster") {
        elements.push(
          <div key={key}>
            <label className="text-muted label-text">{key}:</label>
            <span className="">{value}</span>
          </div>
        );
      }
    }

    return elements;
  };

  const renderRatings = () => {
    if (!movie.Ratings) {
      return;
    }

    let listItems = movie.Ratings.map((rating, index) => {
      return (
        <div key={index}>
          <label className="text-muted label-text">{rating.Source}:</label>
          <span className="">{rating.Value}</span>
        </div>
      );
    });

    return listItems;
  };

  const renderNytMovieReviews = () => {
    if (!nytReviews.results) {
      return <div>No Reviews Found From Nyt.</div>;
    }
    return nytReviews.results.map((review, index) => {
      return (
        <NytMovieReviewItem key={index} review={review} index={index + 1} />
      );
    });
  };

  return (
    <div>
      {showLoader ? (
        <LoadingIndicator />
      ) : (
        <div>
          <div className="card">
            <img
              src={movie.Poster}
              className="card-img-overlay movie-view-img"
              alt={movie.Poster}
            />
            <div className="card-body card-body-width">
              <Link to="/" className="btn btn-primary">
                Back
              </Link>
              <h4>OMDB Data</h4>
              {renderCardData()}
              <h5 className="rating-header">Rating</h5>
              {renderRatings()}
            </div>
          </div>
          <div className="card">
            <div className="card-body card-body-width">
              <h4>NYT - Reviews</h4>
              {renderNytMovieReviews()}
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default MovieView;
