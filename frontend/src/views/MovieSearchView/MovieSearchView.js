import React from "react";
import "./MovieSearchView.css";

import { useSelector, useDispatch } from "react-redux";
import {
  getMovieListByInput,
  getMovieList,
  isLoading,
  getSearchByTitleText,
  getSearchByYearText,
  getSearchByTypeText,
  setSearchByTitleText,
  setSearchByYearText,
  setSearchByTypeText,
} from "../../app/reducers/movieStore";
import MovieList from "../../components/MovieList/MovieList";
import LoadingIndicator from "../../components/LoadingIndicator/LoadingIndicator";
function MovieSearchView() {
  const dispatch = useDispatch();
  const movieList = useSelector(getMovieList);
  const showLoader = useSelector(isLoading);
  const searchText = useSelector(getSearchByTitleText);
  const searchYear = useSelector(getSearchByYearText);
  const searchType = useSelector(getSearchByTypeText);

  function fetchMovieData(e) {
    e.preventDefault();

    if (!isSearchByTitleValid() || !isSearchByYearValid()) {
      return;
    }

    const params = {
      title: searchText,
      year: searchYear,
      type: searchType,
      page: null,
    };

    dispatch(getMovieListByInput(params));
  }

  const isSearchByTitleValid = () => {
    return searchText.length > 0;
  };

  const isSearchByYearValid = () => {
    return /^\d+$/.test(searchYear) || searchYear.length === 0;
  };

  return (
    <div>
      <form className="row g-3">
        <h2>Search Movies</h2>
        <div className="form-floating col-sm-2">
          <input
            type="text"
            className={
              isSearchByTitleValid()
                ? "form-control"
                : "form-control is-invalid"
            }
            id="floatingTitle"
            placeholder="..."
            value={searchText}
            onChange={(e) => dispatch(setSearchByTitleText(e.target.value))}
          />
          <label htmlFor="floatingTitle">
            {isSearchByTitleValid() ? "Title" : "Title is required"}
          </label>
        </div>
        <div className="form-floating col-sm-2">
          <input
            type="text"
            className={
              isSearchByYearValid() ? "form-control" : "form-control is-invalid"
            }
            id="floatingYear"
            placeholder="year"
            value={searchYear}
            onChange={(e) => dispatch(setSearchByYearText(e.target.value))}
          />
          {
            <label htmlFor="floatingYear">
              {isSearchByYearValid() ? "Year" : "Only digits"}
            </label>
          }
        </div>
        <div className="col-sm-2">
          <div className="form-floating">
            <select
              className="form-select"
              id="floatingSelectType"
              aria-label="Floating label select example"
              value={searchType}
              onChange={(e) => dispatch(setSearchByTypeText(e.target.value))}
            >
              <option value="">None</option>
              <option value="movie">Movie</option>
              <option value="series">Series</option>
              <option value="episode">Episode</option>
            </select>
            <label htmlFor="floatingSelectType">Type</label>
          </div>
        </div>
        <div className="col-sm-3">
          <div className="form-floating input-group-lg">
            <button
              type="submit"
              className={
                isSearchByTitleValid() && isSearchByYearValid()
                  ? "btn btn-primary btn-height"
                  : "btn btn-primary btn-height disabled"
              }
              onClick={(e) => fetchMovieData(e)}
            >
              Search
            </button>
          </div>
        </div>
      </form>
      {showLoader ? <LoadingIndicator /> : <MovieList movieList={movieList} />}
    </div>
  );
}

export default MovieSearchView;
