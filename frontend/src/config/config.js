export const API_ROUTES = {
  SEARCH: "http://localhost:3001/search",
  MOVIE: "http://localhost:3001/movie",
  NYT_REVIEW: "http://localhost:3001/nyt-review",
};
