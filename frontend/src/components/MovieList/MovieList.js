import React from "react";
import MovieListItem from "../MovieListItem/MovieListItem";
import Pagination from "../Pagination/Pagination";
import "./MovieList.css";
function MovieList(props) {
  const renderMovies = () => {
    if (!props.movieList.Search) {
      return;
    }

    let listItems = props.movieList.Search.map((movie, index) => {
      return <MovieListItem key={index} movie={movie} />;
    });

    return listItems;
  };
  return (
    <div>
      <h4 className="header-spacing">List Of Movies</h4>
      {renderMovies()}
      <Pagination />
    </div>
  );
}

export default MovieList;
