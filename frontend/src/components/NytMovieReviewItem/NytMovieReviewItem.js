import React from "react";
import "./NytMovieReviewItem.css";

function NytMovieReviewItem(props) {
  const renderReviewCardData = () => {
    if (!props.review) {
      return;
    }

    let elements = [];
    for (const [key, value] of Object.entries(props.review)) {
      if (key !== "link" && key !== "multimedia") {
        let formattedKey = key.replace(/_/g, " ");
        formattedKey = formattedKey.capitalize();

        elements.push(
          <div key={key}>
            <label className="text-muted label-text">{formattedKey}:</label>
            <span className="">{value}</span>
          </div>
        );
      }
    }

    return elements;
  };

  const renderReviewLink = () => {
    if (!props.review.link) {
      return;
    }

    return (
      <a href={props.review.link.url} target="_blank" rel="noreferrer">
        {props.review.link.suggested_link_text}
      </a>
    );
  };

  // eslint-disable-next-line no-extend-native
  String.prototype.capitalize = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
  };

  return (
    <div className="review-padding">
      #{props.index}
      {renderReviewCardData()}
      {renderReviewLink()}
    </div>
  );
}

export default NytMovieReviewItem;
