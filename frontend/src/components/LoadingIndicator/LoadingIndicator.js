import React from "react";
import "./LoadingIndicator.css";
function LoadingIndicator() {
  return (
    <div>
      <div className="spinner-container">
        <div className="spinner-border text-primary spinner-size" role="status">
          <span className="visually-hidden">Loading...</span>
        </div>
      </div>
      <div className="row text-center">
        <strong>Loading...</strong>
      </div>
    </div>
  );
}

export default LoadingIndicator;
