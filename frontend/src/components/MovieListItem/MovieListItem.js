import React from "react";
import "../MovieListItem/MovieListItem.css";
import { Link } from "react-router-dom";

function MovieListItem(props) {
  const getPoster = () => {
    if (props.movie.Poster.length < 5) {
      return (
        <div className="text-center poster-size no-poster-text">
          <p className="">No Poster available</p>
        </div>
      );
    } else {
      return (
        <div className="poster-size">
          <img
            src={props.movie.Poster}
            className="card-img-top center img-fluid"
            alt={props.movie.Poster}
          />
        </div>
      );
    }
  };

  return (
    <Link to={"/" + props.movie.imdbID}>
      <div className="card card-width">
        {getPoster()}
        <div className="card-body">
          <div className="card-title-container">
            <h6 className="card-title">{props.movie.Title}</h6>
          </div>
          <div>
            <label className="card-label card-subtitle mb-2 text-muted">
              Type:
            </label>
            <span>{props.movie.Type}</span>
          </div>
          <div>
            <label className="card-label card-subtitle mb-2 text-muted">
              Year:
            </label>
            <span>{props.movie.Year}</span>
          </div>
        </div>
      </div>
    </Link>
  );
}

export default MovieListItem;
