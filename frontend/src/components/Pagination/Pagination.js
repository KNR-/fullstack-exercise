import React from "react";
import { useSelector, useDispatch } from "react-redux";
import "./Pagination.css";
import {
  getSearchByTitleText,
  getSearchByYearText,
  getSearchByTypeText,
  getMovieListPages,
  getMovieListByInput,
} from "../../app/reducers/movieStore";

function Pagination() {
  const searchText = useSelector(getSearchByTitleText);
  const searchYear = useSelector(getSearchByYearText);
  const searchType = useSelector(getSearchByTypeText);
  const pages = useSelector(getMovieListPages);
  const dispatch = useDispatch();
  const renderPages = () => {
    const elements = [];
    for (let i = 0; i < pages; i++) {
      let page = i + 1;
      elements.push(
        <li key={i} className="page-item">
          <span
            className="page-link"
            onClick={pageClickedHandler.bind(null, page)}
          >
            {page}
          </span>
        </li>
      );
    }
    return elements;
  };

  const pageClickedHandler = (page) => {
    const params = {
      title: searchText,
      year: searchYear,
      type: searchType,
      page: page,
    };
    dispatch(getMovieListByInput(params));
  };

  return (
    <div className="pagination-container">
      <nav aria-label="Page navigation">
        <ul className="pagination">{renderPages()}</ul>
      </nav>
    </div>
  );
}

export default Pagination;
